/****************************************************************************
 *
 *                                 M U E S L I   v 1.5
 *
 *
 *     Copyright 2016 IMDEA Materials Institute, Getafe, Madrid, Spain
 *     Contact: muesli.materials@imdea.org
 *     Author: Ignacio Romero (ignacio.romero@imdea.org)
 *
 *     This file is part of MUESLI.
 *
 *     MUESLI is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MUESLI is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/


#include "johnsoncook.h"
#include <string.h>
#include <cmath>

#define J2TOL     1e-10
#define GAMMAITER 15
#define SQ23      0.816496580927726

using namespace std;
using namespace muesli;


johnsonCookMaterial :: johnsonCookMaterial(const std::string& name,
                                           const materialProperties& cl)
:
finiteStrainMaterial(name, cl),
E(0.0), nu(0.0), lambda(0.0), mu(0.0),
_A(0.0), _B(0.0), _C(0.0), _N(0.0), _edot0(1.0),
_curT(1.0), _refT(1.0), _meltT(1.0)
{
    muesli::assignValue(cl, "young",       E);
    muesli::assignValue(cl, "poisson",     nu);
    muesli::assignValue(cl, "lambda",      lambda);
    muesli::assignValue(cl, "mu",          mu);
    muesli::assignValue(cl, "density",     rho);
    muesli::assignValue(cl, "a",           _A);
    muesli::assignValue(cl, "b",           _B);
    muesli::assignValue(cl, "c",           _C);
    muesli::assignValue(cl, "m",           _M);
    muesli::assignValue(cl, "n",           _N);
    muesli::assignValue(cl, "edot0",       _edot0);
    muesli::assignValue(cl, "reftemp",     _refT);
    muesli::assignValue(cl, "temp",        _curT);
    muesli::assignValue(cl, "melttemp",    _meltT);
    
    
    // E and nu have priority. If they are defined, define lambda and mu
    if (E*E > 0.0)
    {
        lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
        mu     = E/2.0/(1.0+nu);
    }
    else
    {
        nu = 0.5 * lambda / (lambda+mu);
        E  = mu*2.0*(1.0+nu);
    }
    
    // We set all the constants, so that later on all of them can be recovered fast
    bulk = lambda + 2.0/3.0 * mu;
    
    if (rho > 0.0)
    {
        cp = sqrt((lambda+2.0*mu)/rho);
        cs = sqrt(mu/rho);
    }
}




double johnsonCookMaterial :: characteristicStiffness() const
{
    return E;
}




bool johnsonCookMaterial :: check() const
{
    if (mu > 0.0 && lambda+2.0*mu > 0.0) return true;
    else return false;
}




muesli::finiteStrainMP* johnsonCookMaterial :: createMaterialPoint() const
{
    muesli::finiteStrainMP *mp = new johnsonCookMP(*this);
    return mp;
}




double johnsonCookMaterial :: density() const
{
    return rho;
}




// This function is much faster than the one with string property names, since it
// avoids string comparisons. It should be used.
double johnsonCookMaterial :: getProperty(const propertyName p) const
{
    double ret=0.0;
    
    switch (p)
    {
        case PR_LAMBDA:     ret = lambda;   break;
        case PR_MU:         ret = mu;       break;
        case PR_YOUNG:      ret = E;        break;
        case PR_POISSON:    ret = nu;       break;
        case PR_BULK:       ret = bulk;     break;
        case PR_CP:         ret = cp;       break;
        case PR_CS:         ret = cs;       break;
            
        default:
            std::cout << "\n Error in elasticIsotropicMaterial. Property not defined";
    }
    return ret;
}




void johnsonCookMaterial :: print(std::ostream &of) const
{
    of  << "\n Johnson - Cook rate- and temperature-dependent plasticity"
    << "\n   Young modulus:  E      : " << E
    << "\n   Poisson ratio:  nu     : " << nu
    << "\n   Lame constants: Lambda : " << lambda
    << "\n                   Mu     : " << mu
    << "\n   Bulk modulus:   K      : " << bulk
    << "\n   Density                : " << rho;
    if (rho > 0.0)
    {
        of  << "\n   Wave velocities C_p    : " << cp;
        of  << "\n                   C_s    : " << cs;
    }
    of  << "\n   The yield Kirchhoff stress is of the form:"
    << "\n    |tau| - sqrt(2/3) (A+B e^N) (1+C ln (edot/edot0))(1-theta^M)"
    << "\n    theta = (T - T0)/(Tm - T0)";
    
    of  << "\n   A                  : " << _A
    << "\n   B                      : " << _B
    << "\n   C                      : " << _C
    << "\n   N                      : " << _N
    << "\n   dot{eps}_0             : " << _edot0
    << "\n   T                      : " << _curT
    << "\n   T0                     : " << _refT
    << "\n   Tm                     : " << _meltT
    << "\n   M                      : " << _M;
}




void johnsonCookMaterial :: setRandom()
{
    E      = muesli::randomUniform(1.0, 10.0);
    nu     = muesli::randomUniform(0.05, 0.45);
    rho    = muesli::randomUniform(1.0, 100.0);
    _A     = muesli::randomUniform(1.0, 10.0);
    _B     = muesli::randomUniform(1.0, 10.0);
    _C     = muesli::randomUniform(1.0, 10.0);
    _M     = muesli::randomUniform(1.0, 10.0);
    _N     = muesli::randomUniform(1.0, 10.0);
    _edot0 = muesli::randomUniform(1.0, 10.0);
    _meltT = muesli::randomUniform(100.0, 200.0);
    _curT  = muesli::randomUniform(60.0, 80.0);
    _refT  = muesli::randomUniform(60.0, 80.0);
    
    lambda = E*nu/(1.0-2.0*nu)/(1.0+nu);
    mu     = E/2.0/(1.0+nu);
    cp     = sqrt((lambda+2.0*mu)/rho);
    cs     = sqrt(2.0*mu/rho);
    bulk   = lambda + 2.0/3.0 * mu;
}




bool johnsonCookMaterial :: test(std::ostream &of)
{
    bool isok=true;
    setRandom();
    
    muesli::finiteStrainMP* p = this->createMaterialPoint();
    p->setRandom();
    
    isok = p->testImplementation(of);
    delete p;
    return isok;
}




double johnsonCookMaterial :: waveVelocity() const
{
    return cp;
}




johnsonCookMP :: johnsonCookMP(const johnsonCookMaterial &m)
:
finiteStrainMP(m),
theElastoplasticMaterial(m),
iso_n(0.0), iso_c(0.0),
epdot_n(0.0), epdot_c(0.0),
dgamma(0.0)
{
    be_n = be_c = istensor::identity();
    tau.setZero();
    lambda2 = lambda2TR = ivector(1.0, 1.0, 1.0);
    tau.setZero();
    nubarTR.setZero();
    nn[0] = ivector(1.0, 0.0, 0.0);
    nn[1] = ivector(0.0, 1.0, 0.0);
    nn[2] = ivector(0.0, 0.0, 1.0);
}




void johnsonCookMP :: CauchyStress(istensor& sigma) const
{
    const double iJ = 1.0/Jc;
    
    // Reconstruct Cauchy stress
    sigma.setZero();
    for (size_t i=0; i<3; i++)
    {
        sigma.addScaledVdyadicV(iJ * tau[i], nn[i]);
    }
}




void johnsonCookMP :: commitCurrentState()
{
    finiteStrainMP::commitCurrentState();
    
    be_n    = be_c;
    iso_n   = iso_c;
    epdot_n = epdot_c;
}




double johnsonCookMP :: deviatoricEnergy() const
{
    return 0.0;
}




double johnsonCookMP :: dissipatedEnergy() const
{
    const ivector vone(1.0, 1.0, 1.0);
    
    // Logarithmic principal elastic stretches
    ivector  neigvec[3], lambda2n;;
    be_n.spectralDecomposition(neigvec, lambda2n);
    ivector epse_c, epse_n;
    for (size_t i=0; i<3; i++)
    {
        epse_c[i] = 0.5*log(lambda2[i]);
        epse_n[i] = 0.5*log(lambda2n[i]);
    }
    
    ivector devepse_c, devepse_n;
    devepse_c = epse_c - 1.0/3.0 * epse_c.dot(vone)*vone;
    devepse_n = epse_n - 1.0/3.0 * epse_n.dot(vone)*vone;
    
    return 0.0;
}




double johnsonCookMP :: effectiveStoredEnergy() const
{
    return 0.0;
}




void johnsonCookMP :: explicitRadialReturn(const ivector &taudev, double ep, double epdot)
{
    const double mu         = theElastoplasticMaterial.mu;
    const double A          = theElastoplasticMaterial._A;
    const double B          = theElastoplasticMaterial._B;
    const double C          = theElastoplasticMaterial._C;
    const double N          = theElastoplasticMaterial._N;
    const double M          = theElastoplasticMaterial._M;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (1.0 - pow( (Tc-T0)/(Tm-T0) , M ));
    const double edot0      = theElastoplasticMaterial._edot0;
    
    double hard(1.0), dhard(1.0);
    if (ep > 0.0)
    {
        hard  = A + B*pow(ep,N);
        dhard = SQ23*B*N*pow(ep,N-1.0);
    }
    
    double rate(1.0), drate(1.0);
    if (epdot > 0.0)
    {
        rate  = 1.0 + C*log(epdot/edot0);
        drate = C/epdot;
    }
    
    double sigma_y = SQ23 * hard * rate * tempterm;
    dgamma = (taudev.norm() - sigma_y)/(2.0*mu);
    iso_c = ep + SQ23*dgamma*(dhard * rate + hard * drate)*tempterm;
}




double johnsonCookMP :: kineticPotential() const
{
    return 0.0;
}




double johnsonCookMP :: plasticSlip() const
{
    return iso_c;
}




// TaubarTR: trial deviatoric tau.
// Solves dgamma and equivalent plastic strain eqp
void johnsonCookMP :: plasticReturn(const ivector& taubarTR)
{
    const double mu         = theElastoplasticMaterial.mu;
    const double A          = theElastoplasticMaterial._A;
    const double B          = theElastoplasticMaterial._B;
    const double C          = theElastoplasticMaterial._C;
    const double M          = theElastoplasticMaterial._M;
    const double N          = theElastoplasticMaterial._N;
    const double edot0      = theElastoplasticMaterial._edot0;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (1.0 - pow( (Tc-T0)/(Tm-T0) , M ));
    const double dt         = tc - tn;
    
    
    double eqpn = iso_n;
    double ntbar = taubarTR.norm();
    
    // The unknown "x" is delta-gamma
    double x = edot0*dt;
    double G, DG;
    plasticReturnResidual(mu,A,B,C,N,edot0,eqpn,tempterm,ntbar,dt, x, G);
    
    
    // NR scheme algorithm
    size_t count = 0;
    while ( fabs(G) > (J2TOL*A) && ++count < GAMMAITER)
    {
        plasticReturnTangent(mu,A,B,C,N,edot0,eqpn, tempterm, ntbar, dt, x, DG);
        x -= G/DG;
        plasticReturnResidual(mu,A,B,C,N,edot0,eqpn, tempterm, ntbar, dt, x, G);
    }

    
    if (count == GAMMAITER)
    {
        std::cout << "\n Warning: return mapping of Johnson Cook elastoplastic material not converged";
        std::cout << "\n     Residual :" << fabs(G);
    }
    else
    {
        dgamma = x;
        iso_c  = iso_n + SQ23*dgamma;
    }
}




// Calculation of G function value given each iteration value of dgamma
void johnsonCookMP :: plasticReturnResidual(double mu, double A, double B, double C,
                                            double N, double edot0, double eqpn, double tempterm,
                                            double tau, double dt, double dgamma, double& G)
{
    double deqp = SQ23*dgamma;
    double eqp  = eqpn + deqp;
    
    double hard = A;
    if (eqp > 0.0)
    {
        hard += B*pow(eqp,N);
    }
    
    double rate = 1.0;
    if (deqp > 0.0 && dt > 0.0)
    {
        rate += C*log(deqp/(dt*edot0));
    }
    
    G = 2.0*mu*dgamma - tau + SQ23*hard*rate*tempterm;
}




// Calculation of G function derivative value given each iteration value of dgamma
void  johnsonCookMP :: plasticReturnTangent(double mu, double A, double B, double C,
                                            double N, double edot0, double eqpn, double tempterm,
                                            double tau, double dt, double dgamma, double& DG)
{
    double deqp = SQ23*dgamma;
    double eqp  = eqpn + deqp;
    
    double hard = A, dhard = 0.0;
    if (eqp > 0.0)
    {
        hard += B*pow(eqp,N);
        dhard = SQ23*B*N*pow(eqp,N-1.0);
    }
    
    
    double rate = 1.0, drate = 0.0;
    if (deqp > 0.0 && dt > 0.0)
    {
        rate += C*log(deqp/(dt*edot0));
        drate = C/dgamma;
    }
    
    DG = 2.0*mu + SQ23*dhard*rate*tempterm + SQ23*hard*drate*tempterm;
}




void johnsonCookMP :: resetCurrentState()
{
    finiteStrainMP::resetCurrentState();
    
    iso_c   = iso_n;
    be_c    = be_n;
    epdot_c = epdot_n;
}




void johnsonCookMP :: setConvergedState(const double theTime, const itensor& F,
                                        const double iso, const ivector& kine,
                                        const istensor& be)
{
    tn     = theTime;
    Fn     = F;
    Jn     = Fn.determinant();
    iso_n  = iso;
    be_n   = be;
}



void johnsonCookMP :: setRandom()
{
    iso_c = iso_n = muesli::randomUniform(1.0, 2.0);
    
    ivector tmp; tmp.setRandom();
    ivector vone(1.0, 1.0, 1.0);
    
    itensor Fe; Fe.setRandom();
    if (Fe.determinant() < 0.0) Fe *= -1.0;
    be_c = be_n = istensor::tensorTimesTensorTransposed(Fe);
    
    itensor Fp; Fp.setRandom();
    Fp *= 1.0/cbrt(Fp.determinant());
    Fc = Fn = Fe*Fp;
    Jc = Fc.determinant();
}




double johnsonCookMP :: storedEnergy() const
{
    const double lambda = theElastoplasticMaterial.lambda;
    const double mu     = theElastoplasticMaterial.mu;
    
    // Logarithmic principal elastic stretches
    ivector lambda2, xeigvec[3];
    be_c.spectralDecomposition(xeigvec, lambda2);
    ivector epse;
    for (size_t i=0; i<3; i++)
    {
        epse[i] = 0.5*log(lambda2[i]);
    }
    
    const double We = 0.5*lambda*( epse(0) + epse(1) + epse(2) ) * ( epse(0) + epse(1) + epse(2) )
    + mu * epse.squaredNorm();
    double Wp = 0.0;
    return Wp+We;
}




void johnsonCookMP :: updateCurrentState(const double theTime, const istensor& C)
{
    itensor U = istensor::squareRoot(C);
    this->updateCurrentState(theTime, U);
}




void johnsonCookMP :: updateCurrentState(const double theTime, const itensor& F)
{
    finiteStrainMP::updateCurrentState(theTime, F);
    
    // Recover material parameters
    const double mu     = theElastoplasticMaterial.mu;
    const double kappa  = theElastoplasticMaterial.bulk;
    const ivector vone(1.0, 1.0, 1.0);
    
    // Incremental deformation gradient f = F * Fn^-1 */
    itensor f = F * Fn.inverse();
    
    
    //---------------------------------------------------------------------------------------
    //                                     trial state
    //---------------------------------------------------------------------------------------
    // Trial elastic finger tensor b_e
    istensor beTR = istensor::FSFt(f, be_n);
    
    // Logarithmic principal elastic stretches
    beTR.spectralDecomposition(nn, lambda2TR);
    ivector epseTR;
    for (size_t i=0; i<3; i++)
    {
        epseTR[i] = 0.5*log(lambda2TR[i]);
    }
    double theta = epseTR.dot(vone);
    
    // Trial state with frozen plastic flow
    ivector devEpseTR = epseTR - 1.0/3.0*theta*vone;
    double  isoTR     = iso_n;
    
    // Trial deviatoric principal Kirchhoff strees -- Hencky model
    const ivector tauTR = kappa*theta * vone + 2.0*mu* devEpseTR;
    
    // Yield function value at trial state
    double phiTR = yieldFunction(tauTR, isoTR, 0.0);
    
    // Explicit computations, deactivated in this version
    //double edot0 = theElastoplasticMaterial._edot0;
    //double edot = (epdot_n < 1e-4*edot0)  ? 1e-4*edot0 : epdot_n;
    //double phiTR = yieldFunction(tauTR, iso_n, edot);
    
    //---------------------------------------------------------------------------------------
    //                   check trial state and radial return
    //---------------------------------------------------------------------------------------
    ivector devepse_c;
    
    if (phiTR <= 0.0)
    {
        // Elastic step: trial -> n+1
        iso_c     = isoTR;
        devepse_c = devEpseTR;
        dgamma    = 0.0;
    }
    
    else
    {
        // Deviatoric stress
        const ivector devTauTR = 2.0*mu*devEpseTR;
        
        // Plastic step : return mapping in principal stretches space,
        // solution goes into MP variables dgamma and iso_c
        plasticReturn(devTauTR);
        //explicitRadialReturn(devTauTR, iso_n, edot);
        
        
        // Correct the trial quantities
        nubarTR = devTauTR * (1.0/devTauTR.norm());
        devepse_c = devEpseTR - dgamma * nubarTR;
    }
    
    tau = kappa * theta * vone + 2.0 * mu * devepse_c;
    
    // Update elastic finger tensor
    ivector epse_c = devepse_c + 1.0/3.0*theta*vone;
    be_c.setZero();
    for (unsigned i=0; i<3; i++)
    {
        lambda2(i) = exp(2.0*epse_c[i]);
        be_c.addScaledVdyadicV( lambda2(i), nn[i] );
    }
    
    double dt = tc - tn;
    epdot_c = dt > 0.0 ? (iso_c-iso_n)/dt : epdot_n;
    
    // Testing of yield function to test dgamma root solution, deactivated by default
    //double kk = yieldFunction(tau, iso_c, epdot_c);
    //if (fabs(kk)>0.1 && dgamma>0.0)
    //{
    //   std::cout<<"\n"<<kk;
    //   std::cout<<"\n"<<dgamma;
    //}
}




double johnsonCookMP :: volumetricEnergy() const
{
    return 0.0;
}




// Yield function in principal Kirchhoff space
double johnsonCookMP :: yieldFunction(const ivector& tau,
                                      const double&  eps,
                                      const double&  epsdot) const
{
    const double A          = theElastoplasticMaterial._A;
    const double B          = theElastoplasticMaterial._B;
    const double C          = theElastoplasticMaterial._C;
    const double N          = theElastoplasticMaterial._N;
    const double M          = theElastoplasticMaterial._M;
    const double Tc         = theElastoplasticMaterial._curT;
    const double T0         = theElastoplasticMaterial._refT;
    const double Tm         = theElastoplasticMaterial._meltT;
    const double tempterm   = (1.0 - pow( (Tc-T0)/(Tm-T0) , M ));
    
    const double  edot0 = theElastoplasticMaterial._edot0;
    const ivector vone(1.0, 1.0, 1.0);
    
    double hard = A + B*pow(eps,N);
    double rate = (epsdot < J2TOL) ? 1.0 : 1.0 + C*log(epsdot/edot0);
    
    double sigma_y = sqrt(2.0/3.0) * hard * rate * tempterm;
    ivector taubar = tau - (1.0/3.0) * tau.dot(vone)*vone;
    
    return  taubar.norm() - sigma_y;
}

