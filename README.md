# README #

This is MUESLI, a Material UnivErSal LIbrary, a C++ library for modeling material response,
developed at IMDEA Materials Institute, Madrid (Spain). The library is distributed under
GNU GPL3.0 licence (see licence.txt file).

This file corresponds to version 1.5, December 2017.

### Contact
Questions, comments, suggestions, etc., can be addressed to muesli.materials@imdea.org
