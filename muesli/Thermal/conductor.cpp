/****************************************************************************
 *
 *                                 M U E S L I   v 1.5
 *
 *
 *     Copyright 2016 IMDEA Materials Institute, Getafe, Madrid, Spain
 *     Contact: muesli.materials@imdea.org
 *     Author: Ignacio Romero (ignacio.romero@imdea.org)
 *
 *     This file is part of MUESLI.
 *
 *     MUESLI is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     MUESLI is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
 *
*****************************************************************************/



#include "conductor.h"


using namespace std;
using namespace muesli;



conductorMaterial::conductorMaterial(const std::string& name)
:
material(name),
_density(0.0),
_capacity(0.0)
{

}




conductorMaterial::conductorMaterial(const std::string& name,
                                       const materialProperties& cl)
:
material(name, cl),
_density(0.0),
_capacity(0.0)
{
    muesli::assignValue(cl, "density",  _density);
    muesli::assignValue(cl, "capacity", _capacity);
}




bool conductorMaterial::check() const
{
    bool ret = true;

    if (_capacity <= 0.0)
    {
        ret = false;
        std::cout << "Error in conductor material. Non-positive heat capacity";
    }
    
    return ret;
}




double conductorMaterial::density() const
{
    return _density;
}




const double& conductorMaterial::heatCapacity() const
{
    return _capacity;
}




void conductorMaterial::setRandom()
{
    _density      = muesli::randomUniform(1.0, 10.0);
    _capacity     = muesli::randomUniform(1.0, 10.0);
}




conductorMP::conductorMP(const conductorMaterial& mat) :
theConductor(&mat),
time_n(0.0),
time_c(0.0)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void conductorMP::commitCurrentState()
{
    time_n  = time_c;
    temp_n  = temp_c;
    gradT_n = gradT_c;
}




double conductorMP::density() const
{
    return theConductor->density();
}




void conductorMP::resetCurrentState()
{
    time_c  = time_n;
    temp_c  = temp_n;
    gradT_c = gradT_n;
}




void conductorMP::setRandom()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    gradT_n.setRandom();
    gradT_c = gradT_n;

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




bool conductorMP::testImplementation(std::ostream& os) const
{
    bool isok = true;

    double tn1 = muesli::randomUniform(0.1,1.0);

    ivector gradT; gradT.setRandom();
    double temp = muesli::randomUniform(280, 350);
    const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);

    // programmed heatflux and conductivity
    ivector q;    heatflux(q);
    istensor K;   conductivity(K);

    // compare heat flux with (minus) derivative of energy
    if (true)
    {
        // numerical differentiation heatflux
        ivector numFlux;
        numFlux.setZero();
        double   inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            double original = gradT(i);

            gradT(i) = original + inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wp1 = thermalEnergy();

            gradT(i) = original + 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wp2 = thermalEnergy();

            gradT(i) = original - inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wm1 = thermalEnergy();

            gradT(i) = original - 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            double Wm2 = thermalEnergy();


            // fourth order approximation of the derivative
            double der = - (-Wp2 + 8.0*Wp1 - 8.0*Wm1 + Wm2)/(12.0*inc);
            numFlux(i) = der;

            gradT(i) = original;
        }

        // relative error less than 0.01%
        ivector error = numFlux - q;
        isok = (error.norm()/q.norm() < 1e-4);

        os << "\n   1. Comparing heat flux with derivative of thermal energy.";

        if (!isok)
        {
            os << "\n Relative error in q computation %e. Test failed." <<  error.norm()/q.norm();
            os << "\n   Heat flux:\n" << q;
            os << "\n   Numeric heat flux:\n" << numFlux;
        }
        else
            os << " Test passed.";
    }
    

    // compare conductivity with (minus) derivative of heat flux
    if (true)
    {
        istensor numK;
        numK.setZero();
        double   inc = 1.0e-3;

        for (size_t i=0; i<3; i++)
        {
            double original = gradT(i);

            gradT(i) = original + inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp1; heatflux(Qp1);

            gradT(i) = original + 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qp2; heatflux(Qp2);

            gradT(i) = original - inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm1; heatflux(Qm1);

            gradT(i) = original - 2.0*inc;
            const_cast<conductorMP*>(this)->updateCurrentState(tn1, temp, gradT);
            ivector Qm2; heatflux(Qm2);


            // fourth order approximation of the derivative
            ivector der = - (-Qp2 + 8.0*Qp1 - 8.0*Qm1 + Qm2)/(12.0*inc);
            numK(0,i) = der(0);
            numK(1,i) = der(1);
            numK(2,i) = der(2);

            gradT(i) = original;
        }

        // relative error less than 0.01%
        istensor error = numK - K;
        isok = (error.norm()/q.norm() < 1e-4);

        os << "\n   2. Comparing conductivity tensor with derivative of heat flux.";

        if (!isok)
        {
            os << "\n Relative error in q computation %e. Test failed." <<  error.norm()/q.norm();
            os << "\n   Conductivity flux:\n" << K;
            os << "\n   Numeric conductivity :\n" << numK;
        }
        else
            os << " Test passed.";
    }


    return isok;
}




fourierMaterial::fourierMaterial(const std::string& name,
                                   const materialProperties& cl)
:
  conductorMaterial(name, cl),
  _conductivity(0.0)
{
    muesli::assignValue(cl, "conductivity", _conductivity);
}




fourierMaterial::fourierMaterial(const std::string& name, const double k, const double rho)
:
conductorMaterial(name),
_conductivity(k)
{
}




fourierMaterial::fourierMaterial(const std::string& name) :
conductorMaterial(name),
_conductivity(0.0)
{
}




bool fourierMaterial::check() const
{
    bool ret = conductorMaterial::check();

	if (_conductivity <= 0.0)
	{
		ret = false;
		std::cout << "Error in fourier material. Non-positive conductivity";
	}

	return ret;
}




conductorMP* fourierMaterial::createMaterialPoint() const
{
	fourierMP *mp = new fourierMP(*this);
	return mp;
}




double fourierMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    if      (p == PR_CONDUCTIVITY)		ret = _conductivity;
    else if (p == PR_THERMAL_CAP)       ret = heatCapacity();
    else
    {
        std:: cout << "\n Error in GetfourierMaterialProperty.";
    }
    return ret;
}




void fourierMaterial::print(std::ostream &of) const
{
    of  << "\n   Isotropic thermally conducting material "
        << "\n   Conductivity : " << _conductivity
        << "\n   Density      : " << density()
        << "\n   Heat capacity: " << heatCapacity();
}




void fourierMaterial::setRandom()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMaterial::setRandom();
    _conductivity = muesli::randomUniform(1.0, 10.0);

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




bool fourierMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
    return isok;
}




fourierMP::fourierMP(const fourierMaterial& theMaterial)
:
    conductorMP(theMaterial),
    mat(&theMaterial)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void  fourierMP::commitCurrentState()
{
    conductorMP::commitCurrentState();
}




void fourierMP::conductivity(istensor& K) const
{
    K = istensor::identity() * mat->_conductivity;
}




void fourierMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    tg = mat->_conductivity * na.dot(nb);
}




materialState fourierMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState fourierMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




void fourierMP::heatflux(ivector &q) const
{
	q = (- mat->_conductivity) * gradT_c;
}




void fourierMP::resetCurrentState()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP::resetCurrentState();

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




void fourierMP::setRandom()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP::setRandom();

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




double fourierMP::thermalEnergy() const
{
 	return 0.5*mat->_conductivity * gradT_c.dot(gradT_c);
}





void fourierMP::updateCurrentState(double theTime, double temp, const ivector& gradT)
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    time_c  = theTime;
    temp_c  = temp;
    gradT_c = gradT;

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name,
                                                             const materialProperties& cl)
:
conductorMaterial(name, cl)
{
    _K.setZero();
    muesli::assignValue(cl, "kxx", _K(0,0));
}




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name,
                                                             const double kxx, const double kxy, const double kxz,
                                                             const double kyy, const double kyz,
                                                             const double kzz,
                                                             const double rho)
:
conductorMaterial(name)
{
    _K(0,0) = kxx;
    _K(0,1) = _K(1,0) = kxy;
    _K(0,2) = _K(2,0) = kxz;
    _K(1,1) = kyy;
    _K(1,2) = _K(2,1) = kyz;
    _K(2,2) = kzz;
 }




anisotropicConductorMaterial::anisotropicConductorMaterial(const std::string& name) :
conductorMaterial(name)
{
    _K.setZero();
}




bool anisotropicConductorMaterial::check() const
{
    bool ret = conductorMaterial::check();

    ivector ev = _K.eigenvalues();
    if ( ev.min() <= 0.0)
    {
        ret = false;
        std::cout << "Error in anisotropicConductor material. Non-positive conductivity";
    }

    return ret;
}




conductorMP* anisotropicConductorMaterial::createMaterialPoint() const
{
    anisotropicConductorMP *mp = new anisotropicConductorMP(*this);
    return mp;
}




double anisotropicConductorMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    if (p == PR_THERMAL_CAP)       ret = heatCapacity();
    else
    {
        std:: cout << "\n Error in GetanisotropicConductorMaterialProperty.";
    }
    return ret;
}




void anisotropicConductorMaterial::print(std::ostream &of) const
{
    of  << "\n   Anisotropic thermally conducting material "
    << "\n   Conductivity kxx: " << _K(0,0)
    << "\n   Conductivity kxy: " << _K(0,1)
    << "\n   Conductivity kxz: " << _K(0,2)
    << "\n   Conductivity kyy: " << _K(1,1)
    << "\n   Conductivity kyz: " << _K(1,2)
    << "\n   Conductivity kzz: " << _K(2,2)
    << "\n   Density         : " << density()
    << "\n   Heat capacity   : " << heatCapacity();
}




void anisotropicConductorMaterial::setRandom()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMaterial::setRandom();
    _K.setRandom();

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




bool anisotropicConductorMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
    return isok;
}




anisotropicConductorMP::anisotropicConductorMP(const anisotropicConductorMaterial& theMaterial)
:
conductorMP(theMaterial),
mat(&theMaterial)
{
    gradT_n.setZero();
    gradT_c.setZero();
}




void  anisotropicConductorMP::commitCurrentState()
{
    conductorMP::commitCurrentState();
}




void anisotropicConductorMP::conductivity(istensor& K) const
{
    K = mat->_K;
}




void anisotropicConductorMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    tg = na.dot(mat->_K * nb);
}




materialState anisotropicConductorMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState anisotropicConductorMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




void anisotropicConductorMP::heatflux(ivector &q) const
{
    q = - (mat->_K * gradT_c);
}




void anisotropicConductorMP::resetCurrentState()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP::resetCurrentState();

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




void anisotropicConductorMP::setRandom()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP::setRandom();

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




double anisotropicConductorMP::thermalEnergy() const
{
    return 0.5* gradT_c.dot(mat->_K * gradT_c);
}





void anisotropicConductorMP::updateCurrentState(double theTime, double temp, const ivector& gradT)
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    time_c  = theTime;
    temp_c  = temp;
    gradT_c = gradT;
    
#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}



nonlinearConductorMaterial::nonlinearConductorMaterial(const std::string& name,
                                                             const materialProperties& cl)
:
conductorMaterial(name, cl)
{
    muesli::assignValue(cl, "k0", k0);
    muesli::assignValue(cl, "k1", k1);
}




nonlinearConductorMaterial::nonlinearConductorMaterial(const std::string& name, 
                                   const double _k0, const double _k1, 
                                   const double rho)
:
conductorMaterial(name)
{
     k0 = _k0;
     k1 = _k1;
}




nonlinearConductorMaterial::nonlinearConductorMaterial(const std::string& name) :
conductorMaterial(name)
{
     
     k0 = 0;
     k1 = 0;
}





nonlinearConductorMaterial::nonlinearConductorMaterial(const nonlinearConductorMaterial* nlCM) :
conductorMaterial("none")
{
     k0 = nlCM->k0;
     k1 = nlCM->k1; 
}






bool nonlinearConductorMaterial::check() const
{
    bool ret = conductorMaterial::check();

	if (k0 <= 0.0)
	{
		ret = false;
		std::cout << "Error in nonlinear material. Non-positive conductivity at temp=0";
	}
       if (k1 <= 0.0)
	{
		ret = false;
		std::cout << "Error in nonlinear material. Non-positive conductivity";
	}

	return ret;
}




conductorMP* nonlinearConductorMaterial::createMaterialPoint() const
{
	nonlinearConductorMP *mp = new nonlinearConductorMP(*this);
	return mp;
}




double nonlinearConductorMaterial::getProperty(const propertyName p) const
{
    double ret=0.0;

    if (p == PR_THERMAL_CAP)       ret = heatCapacity();
    else
    {
        std:: cout << "\n Error in GetnonlinearMaterialProperty.";
    }
    return ret;
}




void nonlinearConductorMaterial::print(std::ostream &of) const
{
    of  << "\n   Nonlinear thermally conducting material "
        << "\n   Conductivity k0: " << k0
        << "\n   Conductivity k1: " << k1
        << "\n   Density      : " << density()
        << "\n   Heat capacity: " << heatCapacity();
}




void nonlinearConductorMaterial::setRandom()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMaterial::setRandom();

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




bool nonlinearConductorMaterial::test(std::ostream& os)
{
    bool isok = true;
    setRandom();

#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP* p = this->createMaterialPoint();

    isok = p->testImplementation(os);
    delete p;

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
    return isok;
}





nonlinearConductorMP::nonlinearConductorMP(const nonlinearConductorMaterial& theMaterial)
:
    conductorMP(theMaterial),
    mat(&theMaterial)
{
    
    gradT_n.setZero();
    gradT_c.setZero();
}





nonlinearConductorMP::nonlinearConductorMP(const nonlinearConductorMP& theMaterial)
:
    conductorMP(theMaterial)
{
    const nonlinearConductorMaterial* test = theMaterial.mat;
 
    gradT_n.setZero();
    gradT_c.setZero();
}




void  nonlinearConductorMP::commitCurrentState()
{
    conductorMP::commitCurrentState();
}




void nonlinearConductorMP::conductivity(istensor& K) const
{
    double ktheta = mat->k0 + mat->k1 * temp_c;
    K = istensor::identity() * ktheta;
}




void nonlinearConductorMP::contractTangent(const ivector& na, const ivector& nb, double& tg) const
{
    double ktheta = mat->k0 + mat->k1 * temp_c;
    tg = ktheta * na.dot(nb);
}




materialState nonlinearConductorMP::getConvergedState() const
{
    materialState state;

    state.theTime = time_n;
    state.theVector.push_back(gradT_n);

    return state;
}




materialState nonlinearConductorMP::getCurrentState() const
{
    materialState state;

    state.theTime = time_c;
    state.theVector.push_back(gradT_c);

    return state;
}




void nonlinearConductorMP::heatflux(ivector &q) const
{
    double ktheta = mat->k0 + mat->k1 * temp_c;
	q = -ktheta * gradT_c;
}




void nonlinearConductorMP::resetCurrentState()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP::resetCurrentState();

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




void nonlinearConductorMP::setRandom()
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    conductorMP::setRandom();

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}




double nonlinearConductorMP::thermalEnergy() const
{
    double ktheta = mat->k0 + mat->k1 * temp_c;
 	return 0.5 * ktheta * gradT_c.dot(gradT_c);
}





void nonlinearConductorMP::updateCurrentState(double theTime, double temp, const ivector& gradT)
{
#ifdef STRICT_THREAD_SAFE
    theMutex.lock();
#endif

    time_c  = theTime;
    temp_c  = temp;
    gradT_c = gradT;

#ifdef STRICT_THREAD_SAFE
    theMutex.unlock();
#endif
}



